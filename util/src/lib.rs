use std::{fs::File, io::Read};

pub fn read_input(day: u8, is_test: bool) -> String {
    let test_input = if !is_test { "" } else { "test_" };
    let path = format!("day_{:0>2}/{}input.txt", day, test_input);
    let mut input = String::new();
    let mut file = File::open(path).unwrap();
    file.read_to_string(&mut input).unwrap();
    input.to_owned()
}
