use std::collections::HashSet;

use util::read_input;

fn part_one() -> u64 {
    let input = read_input(4, false);

    let mut res = 0;
    for line in input.lines() {
        let (_, game) = line.split_once(": ").unwrap();
        let (winning, chosen) = game
            .split_once(" | ")
            .map(|(w, c)| {
                (
                    w.split(" ")
                        .filter(|n| !n.is_empty())
                        .map(|n| n.parse::<u64>().unwrap())
                        .collect::<Vec<u64>>(),
                    c.split(" ")
                        .filter(|n| !n.is_empty())
                        .map(|n| n.parse::<u64>().unwrap())
                        .collect::<Vec<u64>>(),
                )
            })
            .unwrap();

        let intersect = chosen
            .iter()
            .collect::<HashSet<_>>()
            .intersection(&winning.iter().collect::<HashSet<_>>())
            .map(|val| **val)
            .collect::<Vec<u64>>();

        if intersect.len() > 0 {
            res += 2u64.pow((intersect.len() - 1) as u32);
        }
    }

    res
}
fn part_two() -> u64 {
    let input = read_input(4, false);

    let mut intersections: Vec<Vec<u64>> = Vec::new();
    for line in input.lines() {
        let (_, game) = line.split_once(": ").unwrap();
        let (winning, chosen) = game
            .split_once(" | ")
            .map(|(w, c)| {
                (
                    w.split(" ")
                        .filter(|n| !n.is_empty())
                        .map(|n| n.parse::<u64>().unwrap())
                        .collect::<Vec<u64>>(),
                    c.split(" ")
                        .filter(|n| !n.is_empty())
                        .map(|n| n.parse::<u64>().unwrap())
                        .collect::<Vec<u64>>(),
                )
            })
            .unwrap();

        let intersect = chosen
            .iter()
            .collect::<HashSet<_>>()
            .intersection(&winning.iter().collect::<HashSet<_>>())
            .map(|val| **val)
            .collect::<Vec<u64>>();

        intersections.push(intersect);
    }

    let mut copies: Vec<u64> = vec![1; intersections.len()];
    for (index, inter) in intersections.iter().enumerate() {
        if inter.len() > 0 {
            for i in 1..=inter.len() {
                copies[index + i] += copies[index];
            }
        }
    }

    copies.iter().sum()
}

fn main() {
    println!("result of part one: {}", part_one());
    println!("result of part two: {}", part_two());
}
