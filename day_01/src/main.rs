use ::util::read_input;


//used in part 2
const MAP: [&str; 9] = [
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
];

fn part_one() -> u64 {
    let input = read_input(1, false);
    let mut sum = 0;

    for line in input.lines() {
        let mut first = 0;
        let mut last = 0;
        let mut found_first = false;
        for char in line.chars() {
            if char.is_digit(10) {
                if !found_first {
                    found_first = true;
                    first = char.to_digit(10).unwrap();
                }
                last = char.to_digit(10).unwrap();
            }
        }
        sum += (first * 10 + last) as u64;
    }
    sum
}

fn is_digit(slice: &str) -> Option<u32> {
    let slice_chars = slice.chars().collect::<Vec<char>>();
    if slice_chars[0].is_digit(10) {
        return slice_chars[0].to_digit(10);
    }
    for (number, num_str) in MAP.iter().enumerate() {
        let number = number + 1;
        let num_chars = num_str.chars().collect::<Vec<char>>();
        if slice_chars.len() < num_chars.len() {
            continue;
        }

        let mut is_num = true;
        for i in 0..num_chars.len() {
            if num_chars[i] != slice_chars[i] {
                is_num = false;
                break;
            }
        }
        if is_num {
            return Some(number as u32);
        }
    }
    return None;
}

fn part_two() -> u64 {
    let input = read_input(1, false);
    let mut sum = 0;

    for line in input.lines() {
        let mut first = 0;
        let mut last = 0;
        let mut found_first = false;
        for index in 0..line.chars().count() {
            if let Some(num) = is_digit(&line[index..]) {
                if !found_first {
                    found_first = true;
                    first = num;
                }
                last = num;
            }
        }
        sum += (first * 10 + last) as u64;
    }
    sum
}

fn main() {
    println!("sum of part one: {}", part_one());
    println!("sum of part two: {}", part_two());
}
