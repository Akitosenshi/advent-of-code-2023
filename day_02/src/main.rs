use ::util::read_input;

struct GameSet {
    red: u32,
    green: u32,
    blue: u32,
}

impl From<&str> for GameSet {
    fn from(set_str: &str) -> Self {
        let mut set = GameSet::default();
        for item_str in set_str.split(", ") {
            let (number, color) = item_str.split_once(" ").unwrap();
            match color {
                "red"   => set.red   = number.parse().unwrap(),
                "green" => set.green = number.parse().unwrap(),
                "blue"  => set.blue  = number.parse().unwrap(),
                _ => println!("input must be one of [\"red\", \"green\", \"blue\"] was {color}")
            }
        }
        set
    }
}

impl Default for GameSet {
    fn default() -> Self {
        Self {
            red: Default::default(),
            green: Default::default(),
            blue: Default::default(),
        }
    }
}

struct Game {
    id: u32,
    max: GameSet,
}

const LIMIT: GameSet = GameSet { red: 12, green: 13, blue: 14 };

impl Default for Game {
    fn default() -> Self {
        Self { id: Default::default(), max: Default::default() }
    }
}

impl From<&str> for Game {
    fn from(game_str: &str) -> Self {
        let mut  game = Game::default();
        let (game_str, sets) = game_str.split_once(": ").unwrap();
        game.id = {
            let (_, id) = game_str.split_once(" ").unwrap();
            id.parse().unwrap()
        };

        for set_str in sets.split("; ") {
            let set: GameSet = set_str.into();
            if set.red > game.max.red {
                game.max.red = set.red;
            }
            if set.green > game.max.green {
                game.max.green = set.green;
            }
            if set.blue > game.max.blue {
                game.max.blue = set.blue;
            }
        }
        game
    }
}

fn part_one() -> u32 {
    let input = read_input(2, false);
    let mut sum = 0;
    for game_str in input.lines() {
        let game: Game = game_str.into();
        if game.max.red <= LIMIT.red && game.max.green <= LIMIT.green && game.max.blue <= LIMIT.blue {
            sum += game.id;
        }
    }
    sum
}

fn part_two() -> u32 {
    let input = read_input(2, false);
    let mut power_sum = 0;
    for game_str in input.lines() {
        let game: Game = game_str.into();
        power_sum += game.max.red * game.max.green * game.max.blue;
    }
    power_sum
}

fn main() {
    println!("sum of part one: {}", part_one());
    println!("sum of part two: {}", part_two());
}
