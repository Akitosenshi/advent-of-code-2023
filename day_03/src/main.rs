use util::read_input;

#[derive(Debug)]
struct Point {
    is_star: bool,
    y: usize,
    x: usize,
}

#[derive(Debug)]
struct Number {
    num: u64,
    len: usize,
    is_part_number: bool,
    point: Point,
}

fn part_one() -> u64 {
    let input = read_input(3, false);
    let height = input.lines().count();
    let width = input.lines().next().unwrap().chars().count();

    let field = input
        .lines()
        .collect::<Vec<&str>>()
        .iter()
        .map(|s| s.chars().collect::<Vec<char>>().into_boxed_slice())
        .collect::<Vec<Box<[char]>>>()
        .into_boxed_slice();
    let mut numbers: Vec<Number> = Vec::new();
    let mut symbols: Vec<Point> = Vec::new();

    for y in 0..height {
        let mut x = 0;
        while x < width {
            let c = field[y][x];
            if c == '.' {
            } else if c.is_digit(10) {
                let num_str = field[y][x..width]
                    .iter()
                    .take_while(|c| c.is_digit(10))
                    .collect::<String>();
                let len = num_str.len();
                let num = num_str.parse().unwrap();
                numbers.push(Number {
                    num,
                    len,
                    is_part_number: false,
                    point: Point {
                        is_star: false,
                        y,
                        x,
                    },
                });
                x += len - 1;
            } else {
                symbols.push(Point {
                    is_star: true,
                    y,
                    x,
                });
            }
            x += 1;
        }
    }

    let mut sum = 0;
    for sym in symbols.iter() {
        for num in numbers.iter_mut().filter(|num| !num.is_part_number) {
            let min_y = num.point.y.saturating_sub(1);
            let min_x = num.point.x.saturating_sub(1);
            let max_y = num.point.y + 1;
            let max_x = num.point.x + num.len;
            if sym.y >= min_y && sym.y <= max_y && sym.x >= min_x && sym.x <= max_x {
                num.is_part_number = true;
                sum += num.num;
            }
        }
    }

    sum
}

fn part_two() -> u64 {
    let input = read_input(3, false);
    let height = input.lines().count();
    let width = input.lines().next().unwrap().chars().count();

    let field = input
        .lines()
        .collect::<Vec<&str>>()
        .iter()
        .map(|s| s.chars().collect::<Vec<char>>().into_boxed_slice())
        .collect::<Vec<Box<[char]>>>()
        .into_boxed_slice();
    let mut numbers: Vec<Number> = Vec::new();
    let mut symbols: Vec<Point> = Vec::new();

    for y in 0..height {
        let mut x = 0;
        while x < width {
            let c = field[y][x];
            if c == '.' {
            } else if c.is_digit(10) {
                let num_str = field[y][x..width]
                    .iter()
                    .take_while(|c| c.is_digit(10))
                    .collect::<String>();
                let len = num_str.len();
                let num = num_str.parse().unwrap();
                numbers.push(Number {
                    num,
                    len,
                    is_part_number: false,
                    point: Point {
                        is_star: false,
                        y,
                        x,
                    },
                });
                x += len - 1;
            } else {
                symbols.push(Point {
                    is_star: true,
                    y,
                    x,
                });
            }
            x += 1;
        }
    }

    let mut sum = 0;
    for sym in symbols.iter() {
        let mut found: Vec<u64> = Vec::new();
        for num in numbers.iter_mut().filter(|num| !num.is_part_number) {
            let min_y = num.point.y.saturating_sub(1);
            let min_x = num.point.x.saturating_sub(1);
            let max_y = num.point.y + 1;
            let max_x = num.point.x + num.len;
            if !num.is_part_number
                && sym.y >= min_y
                && sym.y <= max_y
                && sym.x >= min_x
                && sym.x <= max_x
            {
                num.is_part_number = true;
                found.push(num.num);
            }
        }
        // TODO: wrong, too high
        if found.len() == 2 {
            sum += found.iter().product::<u64>();
        }
    }

    sum
}

fn main() {
    println!("result of part one: {}", part_one());
    println!("result of part two: {}", part_two());
}
